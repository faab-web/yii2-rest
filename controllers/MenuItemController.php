<?php

namespace app\controllers;

use Yii;
use app\models\MenuItem;
use app\models\MenuItemSearch;
use yii\web\Controller;
use yii\rest\ActiveController;

class MenuItemController extends ActiveController
{

    public $modelClass = 'app\models\MenuItem';
}
